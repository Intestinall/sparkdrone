import random
import uuid
from socket import socket
from time import sleep
from typing import List

from drone import Drone


def send_drone_data(
    conn: socket, conn2: socket, conn3: socket, drones: List[Drone]
) -> None:
    while True:
        for drone in drones:
            try:
                conn.sendall(drone.csv())
                if random.random() < 0.05:
                    image_id = str(uuid.uuid4())
                    conn2.sendall(drone.csv_violation(image_id))
                    conn3.sendall(drone.csv_image(image_id))
            except (BrokenPipeError, ConnectionResetError):
                return
        sleep(2)

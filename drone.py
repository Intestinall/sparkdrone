import random
import time
import uuid
from dataclasses import dataclass

from images import IMAGES


@dataclass
class Drone:
    _latitude: float
    _longitude: float
    _altitude: float
    id: str

    def __init__(self):
        self._latitude = random.uniform(-90, 90)
        self._longitude = random.uniform(-180, 180)
        self._altitude = random.uniform(10, 50)
        self.id = str(uuid.uuid4())

    @property
    def latitude(self):
        latitude = self._latitude + self._offset()
        if latitude > 90:
            latitude = 90
        elif latitude < -90:
            latitude = -90
        self._latitude = latitude
        return round(self._latitude, 3)

    @property
    def longitude(self):
        longitude = self._longitude + self._offset()
        if longitude > 180:
            longitude -= 360
        elif longitude < -180:
            longitude += 360
        self._longitude = longitude
        return round(self._longitude, 3)

    @property
    def altitude(self):
        altitude = self._altitude + self._offset()
        if altitude > 50:
            altitude = 50
        elif altitude < 10:
            altitude = 10
        self._altitude = altitude
        return round(self._altitude, 3)

    @staticmethod
    def _offset() -> float:
        return random.uniform(-0.1, 0.1)

    def _csv(self, *args) -> bytes:
        return ",".join(str(x) for x in args).encode("utf-8") + b"\n"

    def csv(self) -> bytes:
        return self._csv(
            round(time.time(), 3), self.id, self.altitude, self.latitude, self.longitude
        )

    def csv_violation(self, image_id: str) -> bytes:
        violation_code = random.randint(0, 100)
        return self._csv(
            round(time.time(), 3),
            self.id,
            self.altitude,
            self.latitude,
            self.longitude,
            image_id,
            violation_code,
        )

    def csv_image(self, image_id: str) -> bytes:
        image_data = random.choice(IMAGES)
        return self._csv(
            round(time.time(), 3),
            image_id,
            image_data,
        )

# SparkDrone

Drone emulator for Spark Streaming, all the data are valid, including images.

To use it, configure the `HOST` and `PORT` variables in the `drone_server.py` file.

It will created three port and each one will deliver the following information :
* PORT : localisation data => `timestamp,drone_id,altitude,latitude,longitude`
* PORT + 1 : violation code data => `timestamp,drone_id,altitude,latitude,longitude,image_id,violation_code`
* PORT + 2 : violation image data => `timestamp,image_id,image_in_base64`

Types :
* timestamp : Float
* drone_id : String (UUIDv4)
* altitude : Float
* longitude : Float
* latitude : Float
* image_id : String (UUIDv4)
* violation_code : Integer
* image_in_base64 : String (base64)


import socket

from drone import Drone
from utils import send_drone_data

HOST = "127.0.0.1"  # The server's hostname or IP address
PORT = 65432  # The port used by the server
EVENT_P = 0.05
DRONE_NUMBER = 10
DRONES = [Drone() for _ in range(DRONE_NUMBER)]

if __name__ == "__main__":
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s, socket.socket(
        socket.AF_INET, socket.SOCK_STREAM
    ) as s2, socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s3:
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s2.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s3.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind((HOST, PORT))
        s2.bind((HOST, PORT + 1))
        s3.bind((HOST, PORT + 2))
        s.listen()
        s2.listen()
        s3.listen()
        while True:
            conn, _ = s.accept()
            conn2, _ = s2.accept()
            conn3, _ = s3.accept()
            print("Connection start.")
            with conn, conn2, conn3:
                send_drone_data(conn, conn2, conn3, DRONES)
            print("Connection has ended.")
